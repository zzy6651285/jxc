package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerDao {
    List<Customer> getCustomerList(@Param("startIndex") Integer startIndex, @Param("rows") Integer rows, @Param("customerName") String customerName);

    void update(Customer customer);

    void save(Customer customer);

    void delete(@Param("ids") List<Integer> idsList);
}
