package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OverflowListDao {
    Integer save(OverflowList overflowList);

    List<OverflowList> list(String sTime, String eTime);

}
