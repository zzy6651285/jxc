package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 商品类别
 */
@Repository
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    void save(GoodsType goodsType);

    List<GoodsType> getGoodsTypeById(Integer goodsTypeId);

    List<GoodsType> selectSonTypeBypId(@Param("pId") Integer pId);

    void delete(@Param("goodsTypeId") Integer goodsTypeId);
}
