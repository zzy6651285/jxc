package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @description 商品信息
 */
@Repository
public interface GoodsDao {


    String getMaxCode();

    /**
     * 分页查询首页商品信息
     * @param startIndex
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    List<Goods> getIndexList(@Param("startIndex") Integer startIndex,
                             @Param("rows") Integer rows,
                             @Param("codeOrName") String codeOrName,
                             @Param("goodsTypeId") Integer goodsTypeId);

    /**
     * 分页查询所有商品管理信息
     * @param startIndex
     * @param rows
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    List<Goods> getGoodsList(@Param("startIndex") Integer startIndex,
                             @Param("rows") Integer rows,
                             @Param("goodsName") String goodsName,
                             @Param(("goodsTypeId")) Integer goodsTypeId);

    /**
     * 根据商品类型ID查询该类的所有商品
     * @param sonId
     * @return
     */
    List<Goods> getGoodsListByGoodsTypeId(@Param("sonId") Integer sonId);

    /**
     * 修改商品信息
     * @param goods
     */
    void update(Goods goods);


    /**
     * 添加商品信息
     * @param goods
     */
    void save(Goods goods);

    List<Goods> getGoodsById(Integer goodsId);

    void delete(Integer goodsId);

    List<Goods> getNoInventoryQuantity(@Param("startIndex") Integer startIndex, @Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    List<Goods> getHasInventoryQuantity(@Param("startIndex") Integer startIndex,@Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    void saveStock(@Param("goodsId") Integer goodsId, @Param("inventoryQuantity") Integer inventoryQuantity, @Param("purchasingPrice") Double purchasingPrice);

    void deleteStock(Integer goodsId);

    List<Goods> listAlarm();

}
