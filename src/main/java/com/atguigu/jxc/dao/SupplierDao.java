package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupplierDao {

    List<Supplier> getSupplierList(@Param("startIndex") Integer startIndex, @Param("rows")Integer rows, @Param("supplierName")String supplierName);

    Supplier selectSupplierById(@Param("supplierId") Integer supplierId);

    Integer update(Supplier supplier);

    Integer save(Supplier supplier);

    Integer delete(@Param("ids") List<Integer> strings);
}
