package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.List;
import java.util.Map;

public interface SupplierService {
    Map<String, Object> getSupplierList(Integer page, Integer rows, String supplierName);

    void update(Supplier supplier);

    void save(Supplier supplier);

    void delete(String strings);
}
