package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;

import javax.servlet.http.HttpSession;
import java.util.Map;

public interface OverflowListGoodsService {
    ServiceVO save(OverflowList overflowList, String overflowListGoodsStr, HttpSession session);

    Map<String, Object> list(String sTime, String eTime);

    Map<String, Object> getGoodsList(Integer overflowListId);

}
