package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverflowListDao;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {
    @Autowired
    private OverflowListDao overflowListDao;
    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;
    @Autowired
    private UserDao userDao;

    @Override
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr, HttpSession session) {
        User currentUser = (User) session.getAttribute("currentUser");
        Integer userId = currentUser.getUserId();
        overflowList.setUserId(userId);
        Integer rows = overflowListDao.save(overflowList);

        Gson gson = new Gson();
        List<OverflowListGoods> overflowListGoodsList = gson.fromJson(overflowListGoodsStr, new TypeToken<List<OverflowListGoods>>() {
        }.getType());

        for (OverflowListGoods overflowListGoods : overflowListGoodsList) {
            if (rows > 0){
                overflowListGoods.setOverflowListId(overflowList.getOverflowListId());
            }
            overflowListGoodsDao.save(overflowListGoods);
        }

        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        List<OverflowList> overflowLists = overflowListDao.list(sTime,eTime);
        for (OverflowList overflowList : overflowLists) {
            User userById = userDao.getUserById(overflowList.getUserId());
            overflowList.setTrueName(userById.getTrueName());
        }
        Map<String,Object> map = new HashMap<>();
        map.put("rows", overflowLists);
        return map;
    }

    /**
     * 查询报溢单商品信息
     * @param overflowListId
     * @return
     */
    @Override
    public Map<String, Object> getGoodsList(Integer overflowListId) {
        List<OverflowListGoods> overflowListGoodsList = overflowListGoodsDao.getGoodsList(overflowListId);
        Map<String,Object> map = new HashMap<>();
        map.put("rows", overflowListGoodsList);
        return map;
    }
}
