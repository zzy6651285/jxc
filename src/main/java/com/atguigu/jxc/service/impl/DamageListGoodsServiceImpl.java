package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListGoodService;
import com.atguigu.jxc.service.DamageListService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.shiro.web.session.HttpServletSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DamageListGoodsServiceImpl implements DamageListGoodService {

    @Autowired
    private DamageListGoodsDao damageListGoodsDao;
    @Autowired
    private DamageListDao damageListDao;
    @Autowired
    private UserDao userDao;

    /**
     * 保存报损单
     *
     * @param damageList
     * @param damageListGoodsStr
     * @param session
     * @return
     */
    @Override
    public ServiceVO save(DamageList damageList, String damageListGoodsStr, HttpSession session) {
        User currentUser = (User) session.getAttribute("currentUser");
        Integer userId = currentUser.getUserId();
        damageList.setUserId(userId);
        Integer rows = damageListDao.save(damageList);

        Gson gson = new Gson();
        List<DamageListGoods> damageListGoodsList = gson.fromJson(damageListGoodsStr, new TypeToken<List<DamageListGoods>>() {
        }.getType());

        for (DamageListGoods damageListGoods : damageListGoodsList) {
            if (rows >0){
                damageListGoods.setDamageListId(damageList.getDamageListId());
            }
            damageListGoodsDao.save(damageListGoods);
        }

        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 查询某个时间段的战损单号
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        List<DamageList> damageListsList = damageListDao.list(sTime,eTime);
        for (DamageList damageList : damageListsList) {
            User userById = userDao.getUserById(damageList.getUserId());
            damageList.setTrueName(userById.getTrueName());
        }

        Map<String,Object> map = new HashMap<>();
        map.put("rows", damageListsList);
        return map;
    }

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    @Override
    public Map<String, Object> getGoodsList(Integer damageListId) {
        List<DamageListGoods> damageListGoodsList = damageListGoodsDao.getGoodsList(damageListId);
        Map<String,Object> map = new HashMap<>();
        map.put("rows", damageListGoodsList);
        return map;
    }
}
