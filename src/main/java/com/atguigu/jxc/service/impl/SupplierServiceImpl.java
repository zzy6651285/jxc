package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SupplierServiceImpl implements SupplierService {


    @Autowired
    private SupplierDao supplierDao;

    /**
     * 获取供应商列表
     *
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @Override
    public Map<String, Object> getSupplierList(Integer page, Integer rows, String supplierName) {
        page = page == 0 ? 1 : page;
        int startIndex = (page - 1) * rows;
        List<Supplier> list = supplierDao.getSupplierList(startIndex, rows, supplierName);

        Map<String, Object> map = new HashMap<>();
        map.put("total", list.size());
        map.put("rows", list);

        return map;
    }

    /**
     * 更新供应商信息
     *
     * @param supplier
     * @return
     */
    @Override
    public void update(Supplier supplier) {
        supplierDao.update(supplier);
    }

    /**
     * 添加供应商
     * @param supplier
     */
    @Override
    public void save(Supplier supplier) {
        supplierDao.save(supplier);
    }

    /**
     * 删除供应商
     * @param strIds
     */
    @Override
    public void delete(String strIds) {
        int[] arrayInt = Arrays.stream(strIds.split(","))
                .mapToInt(item -> Integer.parseInt(item))
                .toArray();
        List<Integer> ids = Arrays.stream(arrayInt).boxed().collect(Collectors.toList());

        supplierDao.delete(ids);
    }

    /**
     * 根据ID查询供应商信息
     * @param supplierId
     * @return
     */
    public Supplier selectSupplierById(Integer supplierId) {
        return supplierDao.selectSupplierById(supplierId);
    }
}
