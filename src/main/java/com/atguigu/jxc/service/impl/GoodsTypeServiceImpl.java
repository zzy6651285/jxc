package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.GoodsTypeService;
import com.atguigu.jxc.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description
 */
@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {

    @Autowired
    private LogService logService;
    @Autowired
    private GoodsTypeDao goodsTypeDao;
    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ArrayList<Object> loadGoodsType() {
        logService.save(new Log(Log.SELECT_ACTION, "查询商品类别信息"));


        return this.getAllGoodsType(-1); // 根节点默认从-1开始
    }

    /**
     * 添加商品分类
     *
     * @param goodsTypeName
     * @param pId
     * @return
     */
    @Override
    public ServiceVO save(String goodsTypeName, Integer pId) {
        Integer goodsTypeState = pId <= 1 ? 1 : 0;
        GoodsType goodsType = new GoodsType();
        goodsType.setGoodsTypeName(goodsTypeName);
        goodsType.setPId(pId);
        goodsType.setGoodsTypeState(0);

        List<GoodsType> goodsTypeById = goodsTypeDao.getGoodsTypeById(pId);
        goodsTypeById.get(0).setGoodsTypeState(1);

        goodsTypeDao.save(goodsType);

        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除商品分类
     *
     * @param goodsTypeId
     * @return
     */
    @Override
    public ServiceVO delete(Integer goodsTypeId) {

        List<GoodsType> goodsType = goodsTypeDao.getGoodsTypeById(goodsTypeId);

        if (goodsType.get(0).getPId() == -1){
            return new ServiceVO(ErrorCode.GOODS_TYPE_ERROR_CODE2,ErrorCode.GOODS_TYPE_ERROR_MESS2);
        }

        if (goodsType.get(0).getPId() == 1) {
            List<GoodsType> goodsSonTypeList = goodsTypeDao.selectSonTypeBypId(goodsTypeId);
            if (goodsSonTypeList.size() == 0) {
                goodsTypeDao.delete(goodsTypeId);
                return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            }

            List<Integer> sonIds = goodsSonTypeList.stream()
                    .map(item -> item.getGoodsTypeId())
                    .collect(Collectors.toList());

            for (Integer sonId : sonIds) {
                List<Goods> goodsList = goodsDao.getGoodsListByGoodsTypeId(sonId);
                if (goodsList.size() > 0) {
                    return new ServiceVO(ErrorCode.GOODS_TYPE_ERROR_CODE, ErrorCode.GOODS_TYPE_ERROR_MESS);
                }
            }

            goodsTypeDao.delete(goodsTypeId);
            return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

        }else {
            List<Goods> goodsList = goodsDao.getGoodsListByGoodsTypeId(goodsTypeId);
            if (goodsList.size() > 0) {
                return new ServiceVO(ErrorCode.GOODS_TYPE_ERROR_CODE, ErrorCode.GOODS_TYPE_ERROR_MESS);
            }else {
                goodsTypeDao.delete(goodsTypeId);
                return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            }
        }

    }

    /**
     * 递归查询所有商品类别
     *
     * @return
     */
    public ArrayList<Object> getAllGoodsType(Integer parentId) {

        ArrayList<Object> array = this.getGoodSTypeByParentId(parentId);

        for (int i = 0; i < array.size(); i++) {

            HashMap obj = (HashMap) array.get(i);

            if (obj.get("state").equals("open")) {// 如果是叶子节点，不再递归

            } else {// 如果是根节点，继续递归查询
                obj.put("children", this.getAllGoodsType(Integer.parseInt(obj.get("id").toString())));
            }

        }

        return array;
    }

    /**
     * 根据父ID获取所有子商品类别
     *
     * @return
     */
    public ArrayList<Object> getGoodSTypeByParentId(Integer parentId) {

        ArrayList<Object> array = new ArrayList<>();

        List<GoodsType> goodsTypeList = goodsTypeDao.getAllGoodsTypeByParentId(parentId);

        System.out.println("goodsTypeList" + goodsTypeList);
        //遍历商品类别
        for (GoodsType goodsType : goodsTypeList) {

            HashMap obj = new HashMap<String, Object>();

            obj.put("id", goodsType.getGoodsTypeId());
            obj.put("text", goodsType.getGoodsTypeName());

            if (goodsType.getGoodsTypeState() == 1) {
                obj.put("state", "closed");

            } else {
                obj.put("state", "open");
            }

            obj.put("iconCls", "goods-type");

            HashMap<String, Object> attributes = new HashMap<>();
            attributes.put("state", goodsType.getGoodsTypeState());
            obj.put("attributes", attributes);

            array.add(obj);

        }

        return array;
    }
}
