package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    /**
     * 分页查询客户列表
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @Override
    public Map<String, Object> getCustomerList(Integer page, Integer rows, String customerName) {
        page = page == 0 ? 1 : page;
        Integer startIndex = (page - 1) * rows;

        List<Customer> customerList = customerDao.getCustomerList(startIndex,rows,customerName);

        Map<String,Object> map = new HashMap<>();
        map.put("total",customerList.size());
        map.put("rows",customerList);

        return map;
    }

    /**
     * 添加或修改客户信息
     * @param customer
     * @return
     */
    @Override
    public ServiceVO save(Customer customer) {
        if (customer.getCustomerId() != null){
            customerDao.update(customer);
        }else {
            customerDao.save(customer);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除客户信息
     * @param ids
     * @return
     */
    @Override
    public ServiceVO delete(String ids) {
        int[] array = Arrays.stream(ids.split(","))
                .mapToInt(item -> Integer.parseInt(item))
                .toArray();

        List<Integer> idsList = Arrays.stream(array).boxed().collect(Collectors.toList());

        customerDao.delete(idsList);

        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }
}
