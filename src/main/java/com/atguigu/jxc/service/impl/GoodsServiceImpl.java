package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;
    @Autowired
    private GoodsTypeDao goodsTypeDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    /**
     * 分页查询首页商品库存信息
     * @param page
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    @Override
    public Map<String,Object> getIndexList(Integer page, Integer  rows, String codeOrName, Integer goodsTypeId) {
        page = page == 0 ? 1 : page;
        int startIndex = (page - 1)*rows;

        List<Goods> list = goodsDao.getIndexList(startIndex,rows,codeOrName,goodsTypeId);
        Map<String,Object> map = new HashMap<>();

        map.put("total", list.size());
        map.put("rows", list);

        return map;
    }

    /**
     * 分页查询商品信息
     * @param page
     * @param rows
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    @Override
    public Map<String, Object> getGoodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        page = page == 0 ? 1 : page;
        Integer startIndex = (page -1) * rows;
        List<Goods> list = goodsDao.getGoodsList(startIndex,rows,goodsName,goodsTypeId);
        Map<String,Object> map = new HashMap<>();
        map.put("total", list.size());
        map.put("rows", list);

        return map;
    }

    /**
     * 添加或修改商品信息
     * @param goods
     * @return
     */
    @Override
    public ServiceVO save(Goods goods) {
        if (goods.getGoodsId() != null){
            List<Goods> goodsList = goodsDao.getGoodsById(goods.getGoodsId());
            goods.setInventoryQuantity(goodsList.get(0).getInventoryQuantity());
            goods.setState(goodsList.get(0).getState());
            goodsDao.update(goods);
        }else {
            goods.setInventoryQuantity(0);
            goods.setState(0);
            goodsDao.save(goods);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除没有入库和有进货或销售单据的商品
     * @param goodsId
     * @return
     */
    @Override
    public ServiceVO delete(Integer goodsId) {
        List<Goods> goodsById = goodsDao.getGoodsById(goodsId);
        if (goodsById.get(0).getState() == 1){
            return new ServiceVO(ErrorCode.STORED_ERROR_CODE,ErrorCode.STORED_ERROR_MESS);
        }
        if (goodsById.get(0).getState() == 2){
            return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
        }

        goodsDao.delete(goodsId);

        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 分页查询无库存商品列表
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        page = page == 0 ? 1 : page;
        Integer startIndex = (page - 1) * rows;
        List<Goods> list =  goodsDao.getNoInventoryQuantity(startIndex,rows,nameOrCode);
        Map<String,Object> map = new HashMap<>();
        map.put("total", list.size());
        map.put("rows", list);
        return map;
    }

    /**
     * 分页查询有库存商品的列表
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        page = page == 0 ? 1 : page;
        Integer startIndex = (page -1) * rows;
        List<Goods> list = goodsDao.getHasInventoryQuantity(startIndex,rows,nameOrCode);
        Map<String,Object> map = new HashMap<>();
        map.put("total", list.size());
        map.put("rows", list);
        return map;
    }

    /**
     * 添加库存、修改数量或成本价
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     * @return
     */
    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice) {
        goodsDao.saveStock(goodsId,inventoryQuantity,purchasingPrice);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        List<Goods> goodsById = goodsDao.getGoodsById(goodsId);
        if (goodsById.get(0).getState() == 1){
            return new ServiceVO(ErrorCode.STORED_ERROR_CODE,ErrorCode.STORED_ERROR_MESS);
        }
        if (goodsById.get(0).getState() == 2){
            return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
        }
        goodsDao.deleteStock(goodsId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> listAlarm() {
        List<Goods> list = goodsDao.listAlarm();
        Map<String,Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }


}
