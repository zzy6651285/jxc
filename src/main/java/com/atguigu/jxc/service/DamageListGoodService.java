package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;

import javax.servlet.http.HttpSession;
import java.util.Map;

public interface DamageListGoodService {
    ServiceVO save(DamageList damageList, String damageListGoodsStr, HttpSession session);

    Map<String, Object> list(String sTime, String eTime);

    Map<String, Object> getGoodsList(Integer damageListId);
}
