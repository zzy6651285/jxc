package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageListGoodService;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.web.session.HttpServletSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {

    @Autowired
    private DamageListGoodService damageListGoodService;


    /**
     * 保存报损单
     *
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @PostMapping("/save")
    public ServiceVO save(DamageList damageList, String damageListGoodsStr, HttpSession session) {
        return damageListGoodService.save(damageList, damageListGoodsStr,session);
    }

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> list(String sTime,String eTime){
        return damageListGoodService.list(sTime,eTime);
    }

    @PostMapping("/goodsList")
    public Map<String,Object> goodsList(Integer damageListId){
        return damageListGoodService.getGoodsList(damageListId);
    }

}
