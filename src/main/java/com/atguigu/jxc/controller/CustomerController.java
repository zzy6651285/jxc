package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    /**
     * 获取客户列表
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> getCustomerList(@RequestParam(value = "page") Integer page,
                                          @RequestParam("rows") Integer rows,
                                          @RequestParam(value = "customerName",required = false) String customerName){

        return customerService.getCustomerList(page,rows,customerName);

    }

    /**
     * 添加或修改客户信息
     * @param customer
     * @return
     */
    @PostMapping("save")
    public ServiceVO save(Customer customer){
        return customerService.save(customer);
    }

    /**
     * 删除客户信息
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO delete(String ids){
       return customerService.delete(ids);
    }
}
