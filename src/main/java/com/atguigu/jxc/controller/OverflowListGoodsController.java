package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListGoodsController {

    @Autowired
    private OverflowListGoodsService overflowListGoodsService;

    /**
     * 新增报溢单
     *
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    @PostMapping("/save")
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr, HttpSession session) {
        return overflowListGoodsService.save(overflowList, overflowListGoodsStr, session);
    }

    /**
     * 查询某个时间段内的报溢单子
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("/list")
    public Map<String, Object> list(String sTime, String eTime) {
        return overflowListGoodsService.list(sTime, eTime);
    }

    /**
     * 查询报溢单商品信息
     * @param overflowListId
     * @return
     */
    @PostMapping("/goodsList")
    public Map<String,Object> goodsList(Integer overflowListId){
        return overflowListGoodsService.getGoodsList(overflowListId);
    }

}
