package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;


    /**
     * 分页查询供应商信息
     *
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @PostMapping("/list")
    public Map<String, Object> getSupplierList(@RequestParam(value = "page",required = false) Integer page,
                                               @RequestParam(value = "rows",required = false) Integer rows,
                                               @RequestParam(value = "supplierName",required = false) String supplierName) {

        Map<String, Object> map =  supplierService.getSupplierList(page, rows, supplierName);

        return map;
    }

    /**
     * 添加或修改供应商
     * @param supplier
     * @return
     */
    @PostMapping("/save")
    public ServiceVO saveSupplier(Supplier supplier){
        if (supplier.getSupplierId() != null){
            supplierService.update(supplier);
        }else {
            supplierService.save(supplier);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @PostMapping("/delete")
    public ServiceVO delete(@RequestParam("ids") String ids){

        supplierService.delete(ids);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

}
