package com.atguigu.jxc;


import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
@Slf4j
public class JxcApplicationTests {

	@Test
	public void contextLoads() {
		String code = "123";
		String name = "哈哈哈";

		try {
			int code1 = Integer.parseInt(code);
			log.info("==================> ====================> =======> : " + code1);
		}catch (NumberFormatException e){
			String goodsName = "名字1";
			log.info("==================> ====================> =======> : " + goodsName);
		}

		try {
			int name1 = Integer.parseInt(name);
			log.info("==================> ====================> =======> : " + name1);
		}catch (NumberFormatException e){
			String goodsName = "名字2";
			log.info("==================> ====================> =======> : " + goodsName);
		}

	}

	@Test
	public void test01(){
		String strs = "4,6,7,8,9,10,11";
		int[] arrayInt = Arrays.stream(strs.split(","))
				.mapToInt(item -> Integer.parseInt(item))
				.toArray();
//		List<Integer> ids = Arrays.stream(arrayInt).boxed().collect(Collectors.toList());

		List<Object> objects = new ArrayList<>();

		for (int i : arrayInt) {
			objects.add(i);
		}
		System.out.println(objects);
	}

}
